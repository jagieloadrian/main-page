# Adrian Jagielo

## About Me

 Hi,
 I am a Software Developer, specializing in Kotlin and JVM backend
 development, with high curiosity and willingness to learn different
 technologies, also I have a lot of fun to create some applications :)

## Interests

* Video games 
* Motorcycles
* Cooking

## Contact

![email logo](Untitled.png){ width=30} email: [jagielo.adrian@gmail.com](mailto:jagielo.adrian@gmail.com)

![linkedin logo](linkedin.png){ width=30} LinkedIn: [Adrian Jagielo](https://www.linkedin.com/in/jagieloadrian/)

![ps icon](ps_icon.png){ width=30} psnprofiles: [Sirdiether18](https://psnprofiles.com/Sirdiether18)

![d18_square](d18_square.png){ width=30} playstore: [diether18](https://play.google.com/store/apps/developer?id=diether18)