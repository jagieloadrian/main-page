# My Apps

## Star Wars Wiki Compose

![SW Wiki Logo Icon](sw_compose_icon_round.png)

This is my first big project which I want to share with the world.
I use external apis to fetch data about Star Wars and images search by key-words :)
It's deployed on Google Play Store ;)

Link to the repo: [SW Repo](https://gitlab.com/jagieloadrian/starwarswikicompose)

Link to the app on Play Store: [SW Wiki](https://play.google.com/store/apps/details?id=com.anjo.starwarswikicompose)

## FileBrowser Api

![Kotlin and Spring](spring_kotlin.png){ width=290 }{border-effect=line}

This is my private project. In time when I was looking for docker container to manage files 
for another service. It used spring and kotlin libraries

Link to the repo: [FilebrowserAPI](https://gitlab.com/jagieloadrian/filebrowserapi)

## DatabaseSchedulerExecutor

![DatabaseSchedulerExecutor](dse_logo.png){ width=290 }{border-effect=line}

Kotlin's application to schedule and run background task to execute SQL statement in file `app.properties`

Link to the repo: [DatabaseScheduleExecutor](https://gitlab.com/jagieloadrian/databasescheduleexecutor)

## TuyaKeyExtractorKt

![TuyaKeyExtractorIcon](tuya-key-extractor-icon.png){ width=290 }

Influenced by Mark Watt to write my own implementation of extractor of local keys for tuya devices.
I needed this program to run on different system than Windows, so I think java is the most universal language/engine
for this purpose. 


Link to the repo: [TuyaKeyExtractorKt](https://gitlab.com/jagieloadrian/tuyakeyextractorkt)