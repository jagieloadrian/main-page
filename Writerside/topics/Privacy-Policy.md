# Privacy-Policy

**D18 Apps** takes your privacy seriously. To better protect your privacy **D18 Apps** provide this privacy policy notice explaining the way your personal information is collected and used.

## Collection of Routine Information

This **D18 Apps** doesn't track information about their users. 

## Cookies

Where necessary, this **D18 Apps** uses cookies to store information about a visitor’s preferences and history in order to better serve the users and/or present the users with customized content.

## Advertisement and Other Third Parties

Advertising partners and other third parties may use cookies, scripts and/or web beacons to track users activities on this **D18 Apps** in order to display advertisements and other useful information. Such tracking is done directly by the third parties through their own servers and is subject to their own privacy policies. This **D18 Apps** has no access or control over these cookies, scripts and/or web beacons that may be used by third parties. Learn how to [opt out of Google’s cookie usage](http://www.google.com/privacy_ads.html).

## Links to Third Party Websites

**D18 Apps** have included links on this **D18 Apps** for your use and reference. **D18 Apps** are not responsible for the privacy policies on these websites. You should be aware that the privacy policies of these websites may differ from my own.

## Security

The security of your personal information is important to me, but remember that no method of transmission over the Internet, or method of electronic storage, is 100% secure. While **D18 Apps** strive to use commercially acceptable means to protect your personal information, **D18 Apps** cannot guarantee its absolute security.

## Changes To This Privacy Policy
This Privacy Policy is effective as of 28.02.2024 and will remain in effect except with respect to any changes in its provisions in the future, which will be in effect immediately after being posted on this page.
**D18 Apps** reserve the right to update or change my Privacy Policy at any time, so you should check this Privacy Policy periodically. If **D18 Apps** make any material changes to this Privacy Policy, **D18 Apps** will notify you either through the email address you have provided me, or by placing a prominent notice on my **D18 Apps**.

## Contact Information

For any questions or concerns regarding the privacy policy, please send to me an email to *diether18.apps@gmail.com*.